package engineering.nxt.rockpaperscisors.enum

enum class Weekday {
  Monday, Tuesday,
  Wednesday, Thursday,
  Friday, Saturday,
  Sunday
}

fun main() {
  println(Weekday.values().joinToString(", "))

  var monday = Weekday.Monday
  println("Monday is '$monday'.")

  val weekday = Weekday.values().random()
  println("It's '$weekday'.")

  when(Weekday.Monday == weekday) {
    true -> println("What!? Already Monday?!")
    false -> println("Indeed, it's not yet Monday.")
  }

  val whatPeopleMightSay = when(weekday) {
    Weekday.Monday -> "It's the first day of the week."
    Weekday.Tuesday -> "It's already the second weekday."
    Weekday.Wednesday -> "Only two days of work left."
    Weekday.Thursday -> "Soon it's weekend. Just one more day."
    Weekday.Friday -> "Weekend is almos here. Ready to party?"
    Weekday.Saturday -> "Hangover?"
    Weekday.Sunday -> "You better relax, tomorrow's Monday again!"
  }
  println(whatPeopleMightSay)

  val isItWeekendYet = when(weekday) {
    Weekday.Saturday, Weekday.Sunday -> true
    else -> false
  }
  println("Is it weekend yet? $isItWeekendYet")
}
