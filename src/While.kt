// While.kt
fun main() {
    println("I will choose one of 'a', 'b' or 'c'.")
    val computerChoice = arrayOf("a", "b", "c").random()

    print("Can you guess what I've chosen? ")
    var playerChoice = readLine()

    while (playerChoice != computerChoice) {
        print("Can you guess what I've chosen? ")
        playerChoice = readLine()
    }

    println("Yes, I've thought of '$computerChoice'.")
}
