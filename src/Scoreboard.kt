package engineering.nxt.rockpaperscisors.scoreboard

// Scoreboard.kt
fun main() {
  val gameScore = Scoreboard()
  gameScore.printScore()
  gameScore.increaseComputerScore()
  gameScore.printScore()
  gameScore.increasePlayerScore()
  gameScore.printScore()
}

class Scoreboard {
  var computerScore = 0
  var playerScore = 0

  fun printScore() {
    println("The computer won $computerScore times and the player $playerScore times.")
  }

  fun increaseComputerScore() {
    computerScore = computerScore + 1
  }

  fun increasePlayerScore() {
    playerScore = playerScore + 1
  }
}
