// DiceRollStraight.kt
fun main() {
    do {
        // roll dice
        val diceRoll = arrayOf(1,2,3,4,5,6).random()
        println("The dice shows $diceRoll.")

        // One more time?
        print("Roll the dice again? Yes [y] or No [n]? ")
    } while (readLine() == "y")
}
