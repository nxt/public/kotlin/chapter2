package engineering.nxt.rockpaperscisors.simplescoreboard

// SimpleScoreBoard.kt
class SimpleScoreBoard {
  var countA = 0
  var countB = 0

  fun print() = println("The score is $countA vs $countB.")
}

fun main() {
  val scores = SimpleScoreBoard()
  do {
    print("Who has won? A or B? ")
    val choice = readLine()
    when (choice) {
      "A", "a" -> scores.countA = scores.countA + 1
      "B", "b" -> scores.countB = scores.countB - 1
      else -> println("Unknown choice '$choice'.")
    }
    scores.print()

    print("Again? Type 'y' for yes: ")
    val again = readLine()
  } while (again == "y")
}
