// WordAnalyzer.kt
fun main() {
    println("Please enter two words:")
    val first = readLine().orEmpty()
    val second = readLine().orEmpty()

    // compare
    val firstLen = first.length
    val secondLen = second.length
    println("The first word is $firstLen characters long.")
    println("The second word is $secondLen characters long.")
    if (firstLen < secondLen) {
        val difference = secondLen - firstLen
        println("'$second' is $difference characters longer than '$first'.")
    } else {
        val difference = firstLen - secondLen
        println("'$first' is $difference characters longer than '$second'.")
    }
}
