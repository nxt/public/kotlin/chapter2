// Filename: Dice.kt
import kotlin.random.Random
import kotlin.random.nextInt

fun main() {
    println("Rolling the dice...")
    val result = Random.nextInt(1..6)
    println("The dice stopped and shows $result.")
}
