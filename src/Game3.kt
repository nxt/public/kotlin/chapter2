package engineering.nxt.rockpaperscisors.pt3

// Game.kt
fun main() {
  print("Hello. Please choose between Rock [r], Paper [p] or Scissors [s]: ")
  val userCommand = readLine()
  val userChoice =
    when (userCommand) {
      "r" -> "rock"
      "p" -> "paper"
      "s" -> "scissors"
      else -> "unknown"
    }
  println("You've chosen $userChoice.")

  val choices = arrayOf("rock", "paper", "scissors")
  val computerChoice = choices.random()
  println("The computer has chosen $computerChoice.")
}
