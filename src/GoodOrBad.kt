// Filename: GoodOrBad.kt
fun main() {
    print("How are you? Type 'g' for good or 'b' for bad: ")
    val stateOfMind = readLine()

    val response = when(stateOfMind) {
        "g" -> "Glad to hear you're doing good!"
        "b" -> "Sorry to hear, I hope live turns to the better soon!"
        else -> "Sorry, I don't understand what you mean."
    }

    println(response)
}
