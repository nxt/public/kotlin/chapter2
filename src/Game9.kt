package engineering.nxt.rockpaperscisors.pt9

// Game.kt
class Scoreboard {
  private var computerScore = 0
  private var playerScore = 0

  fun printScore() {
    println("The computer won $computerScore times and the player $playerScore times.")
  }

  fun increaseComputerScore() {
    computerScore = computerScore + 1
  }

  fun increasePlayerScore() {
    playerScore = playerScore + 1
  }
}

class Game {
  private val score = Scoreboard()

  fun playARound() {
    val userChoice = playerDraw()
    val computerChoice = computerDraw()

    referee(userChoice, computerChoice)

    score.printScore()
  }

  private fun playerDraw(): String {
    print("Hello. Please choose between Rock [r], Paper [p] or Scissors [s]: ")
    val userCommand = readLine()
    val userChoice =
      when (userCommand) {
        "r" -> "rock"
        "p" -> "paper"
        "s" -> "scissors"
        else -> "unknown"
      }
    println("You've chosen $userChoice.")

    return userChoice
  }

  private fun computerDraw(): String {
    val choices = arrayOf("rock", "paper", "scissors")
    val computerChoice = choices.random()
    println("The computer has chosen $computerChoice.")

    return computerChoice
  }

  private fun referee(userChoice: String, computerChoice: String) {
    val result = when(userChoice) {
      "rock" -> when(computerChoice) {
        "rock" -> "It's a tie"
        "paper" -> {
          score.increaseComputerScore()
          "Paper beats rock, the computer wins."
        }
        "scissors" -> {
          score.increasePlayerScore()
          "Rock beats scissors, the player wins."
        }
        else -> throw RuntimeException("Unknown choice.")
      }
      "paper" -> when(computerChoice) {
        "rock" -> {
          score.increasePlayerScore()
          "Paper beats rock, the player wins."
        }
        "paper" -> "It's a tie"
        "scissors" -> {
          score.increaseComputerScore()
          "Scissors beat paper, the computer wins."
        }
        else -> throw RuntimeException("Unknown choice.")
      }
      "scissors" -> when(computerChoice) {
        "rock" -> {
          score.increaseComputerScore()
          "Rock beats scissors, the computer wins."
        }
        "paper" -> {
          score.increasePlayerScore()
          "Scissors beat paper, the player wins."
        }
        "scissors" -> "It's a tie"
        else -> throw RuntimeException("Unknown choice.")
      }
      else -> throw RuntimeException("Unknown choice.")
    }

    println(result)
  }
}

fun main() {
  val game = Game()

  do {
    game.playARound()

    // One more time?
    print("Would you like to play again? Type 'y' for yes and 'n' for no. ")
    val againResponse = readLine()
  } while (againResponse == "y")
}
