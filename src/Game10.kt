package engineering.nxt.rockpaperscisors.pt10

// Game.kt
class Scoreboard {
  private var computerScore = 0
  private var playerScore = 0

  fun printScore() {
    println("The computer won $computerScore times and the player $playerScore times.")
  }

  fun increaseComputerScore() {
    computerScore = computerScore + 1
  }

  fun increasePlayerScore() {
    playerScore = playerScore + 1
  }
}

class Game {
  private val score = Scoreboard()

  fun playARound() {
    val userChoice = playerDraw()
    val computerChoice = computerDraw()

    referee(userChoice, computerChoice)

    score.printScore()
  }

  enum class Choice {
    Rock, Paper, Scissors
  }

  private fun playerDraw(): Choice {
    print("Hello. Please choose between Rock [r], Paper [p] or Scissors [s]: ")
    val userCommand = readLine()
    val userChoice =
      when (userCommand) {
        "r" -> Choice.Rock
        "p" -> Choice.Paper
        "s" -> Choice.Scissors
        else -> throw RuntimeException("Unknown choice.")
      }
    println("You've chosen $userChoice.")

    return userChoice
  }

  private fun computerDraw(): Choice {
    val choices = Choice.values()
    val computerChoice = choices.random()
    println("The computer has chosen $computerChoice.")

    return computerChoice
  }

  enum class Winner {
    NoOne, Computer, Player
  }

  private fun referee(userChoice: Choice, computerChoice: Choice) {
    val winner = when (userChoice) {
      Choice.Rock -> rockGiven(computerChoice)
      Choice.Paper -> paperGiven(computerChoice)
      Choice.Scissors -> scissorsGiven(computerChoice)
    }

    val result = when (winner) {
      Winner.NoOne -> "It's a tie."
      Winner.Player -> {
        score.increasePlayerScore()
        "$userChoice beats $computerChoice, the player wins."
      }
      Winner.Computer -> {
        score.increaseComputerScore()
        "$computerChoice beats $userChoice, the computer wins."
      }
    }

    println(result)
  }

  private fun rockGiven(computerChoice: Choice): Winner {
    return when (computerChoice) {
      Choice.Rock -> Winner.NoOne
      Choice.Paper -> Winner.Computer
      Choice.Scissors -> Winner.Player
    }
  }

  private fun paperGiven(computerChoice: Choice): Winner {
    return when (computerChoice) {
      Choice.Rock -> Winner.Player
      Choice.Paper -> Winner.NoOne
      Choice.Scissors -> Winner.Computer
    }
  }

  private fun scissorsGiven(computerChoice: Choice): Winner {
    return when (computerChoice) {
      Choice.Rock -> Winner.Computer
      Choice.Paper -> Winner.Player
      Choice.Scissors -> Winner.NoOne
    }
  }
}

fun main() {
  val game = Game()

  do {
    game.playARound()

    // One more time?
    print("Would you like to play again? Type 'y' for yes and 'n' for no. ")
    val againResponse = readLine()
  } while (againResponse == "y")
}
