package engineering.nxt.rockpaperscisors.privatescoreboard

// PrivateScoreBoard.kt
class PrivateScoreBoard {
  private var countA = 0
  private var countB = 0

  fun increaseCountA() {
    countA = countA + 1
  }

  fun increaseCountB() {
    countB = countB + 1
  }

  fun print() = println("The score is $countA vs $countB.")
}

fun main() {
  val scores = PrivateScoreBoard()
  do {
    print("Who has won? A or B? ")
    val choice = readLine()
    when (choice) {
      "A", "a" -> scores.increaseCountA()
      "B", "b" -> scores.increaseCountB()
      else -> println("Unknown choice '$choice'.")
    }
    scores.print()

    print("Again? Type 'y' for yes: ")
    val again = readLine()
  } while (again == "y")
}
