// GuessTheNumber.kt
import kotlin.random.Random

class GuessTheNumber {
  val number = Random.nextInt(0, 43)
  var guesses = 0

  fun isIt(guess: Int): Boolean {
    guesses = guesses + 1
    return number == guess
  }
}

fun main() {
  val guessGame = GuessTheNumber()

  println("Guess the number:")

  do {
    val guessAsString = readLine()
    val guessAsInt = guessAsString.orEmpty().toInt()
  } while (!guessGame.isIt(guessAsInt))

  println("You needed ${guessGame.guesses} guesses.")
}
