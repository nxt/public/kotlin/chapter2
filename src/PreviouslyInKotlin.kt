// PreviouslyInKotlin.kt
fun main() {
    print("Enter 'y' for Yes or 'n' for No: ")
    val input = readLine()
    val output =
        when(input) {
            "y" -> "Yes, Yes, Yes!"
            "n" -> "Oh no! Why!?"
            else -> "Sorry, I didn't hear you. Could you say that again?"
        }
    println(output)
}
