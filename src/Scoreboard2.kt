package engineering.nxt.rockpaperscisors.scoreboard2

// Scoreboard2.kt
fun main() {
  val gameScore1 = Scoreboard()
  val gameScore2 = Scoreboard()

  print("Board 1: ")
  gameScore1.printScore()
  print("Board 2: ")
  gameScore2.printScore()

  gameScore1.increaseComputerScore()

  print("Board 1: ")
  gameScore1.printScore()
  print("Board 2: ")
  gameScore2.printScore()

  gameScore2.increaseComputerScore()

  print("Board 1: ")
  gameScore1.printScore()
  print("Board 2: ")
  gameScore2.printScore()

  gameScore1.increasePlayerScore()

  print("Board 1: ")
  gameScore1.printScore()
  print("Board 2: ")
  gameScore2.printScore()

  gameScore2.increasePlayerScore()

  print("Board 1: ")
  gameScore1.printScore()
  print("Board 2: ")
  gameScore2.printScore()
}

class Scoreboard {
  var computerScore = 0
  var playerScore = 0

  fun printScore() {
    println("The computer won $computerScore times and the player $playerScore times.")
  }

  fun increaseComputerScore() {
    computerScore = computerScore + 1
  }

  fun increasePlayerScore() {
    playerScore = playerScore + 1
  }
}
