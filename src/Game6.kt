package engineering.nxt.rockpaperscisors.pt6

// Game.kt
fun playerDraw(): String {
  print("Hello. Please choose between Rock [r], Paper [p] or Scissors [s]: ")
  val userCommand = readLine()
  val userChoice =
    when (userCommand) {
      "r" -> "rock"
      "p" -> "paper"
      "s" -> "scissors"
      else -> "unknown"
    }
  println("You've chosen $userChoice.")
  return userChoice
}

fun computerDraw(): String {
  val choices = arrayOf("rock", "paper", "scissors")
  val computerChoice = choices.random()
  println("The computer has chosen $computerChoice.")

  return computerChoice
}

fun main() {
  do {
    val userChoice = playerDraw()
    val computerChoice = computerDraw()

    // referee
    val result = when (userChoice) {
      "rock" -> when (computerChoice) {
        "rock" -> "It's a tie"
        "paper" -> "Paper beats rock, the computer wins."
        "scissors" -> "Rock beats scissors, the player wins."
        else -> "It's unclear if '$userChoice' beats '$computerChoice', or not."
      }
      "paper" -> when (computerChoice) {
        "rock" -> "Paper beats rock, the player wins."
        "paper" -> "It's a tie"
        "scissors" -> "Scissors beat paper, the computer wins."
        else -> "It's unclear if '$userChoice' beats '$computerChoice', or not."
      }
      "scissors" -> when (computerChoice) {
        "rock" -> "Rock beats scissors, the computer wins."
        "paper" -> "Scissors beat paper, the player wins."
        "scissors" -> "It's a tie"
        else -> "It's unclear if '$userChoice' beats '$computerChoice', or not."
      }
      else -> "It's unclear if '$userChoice' beats '$computerChoice', or not."
    }

    println(result)
    // One more time?
    print("Would you like to play again? Type 'y' for yes and 'n' for no. ")
    val againResponse = readLine()
  } while (againResponse == "y")
}
