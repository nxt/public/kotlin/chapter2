package engineering.nxt.rockpaperscisors.dicerollreturnfunction

// DiceRoll.kt
fun rollDice() {
    val diceRoll = arrayOf(1,2,3,4,5,6).random()
    println("The dice shows $diceRoll.")
}

fun oneMoreTime(): Boolean {
    print("Roll the dice again? Yes [y] or No [n]? ")
    val answer = readLine()
    val playOneMoreTime = (answer == "y")
    return playOneMoreTime
}

fun main() {
    do {
        rollDice()
    } while (oneMoreTime())
}
