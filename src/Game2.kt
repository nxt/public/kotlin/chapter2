package engineering.nxt.rockpaperscisors.pt2

// Game.kt
fun main() {
  println("Hello.")
  print("Please choose between Rock [r], Paper [p] or Scissors [s]: ")
  val userCommand = readLine()
  val userChoice =
    when (userCommand) {
      "r" -> "rock"
      "p" -> "paper"
      "s" -> "scissors"
      else -> "unknown"
    }
  println("You've chosen $userChoice.")
}
