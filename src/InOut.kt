// Filename: InOut.kt
fun main() {
    print("What's your name? ")
    val name = readLine()
    println("Hello, $name. How are you?")
}
