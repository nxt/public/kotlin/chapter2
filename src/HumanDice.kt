// Filename: HumanDice.kt
import kotlin.random.Random
import kotlin.random.nextInt

fun main() {
    println("Rolling the dice...")
    val resultNumber  = Random.nextInt(1..6)
    val resultWord    =
        when(resultNumber) {
            1 -> "one"
            2 -> "two"
            3 -> "three"
            4 -> "four"
            5 -> "five"
            6 -> "six"
            else -> throw RuntimeException("Unknown Number")
        }
    println("The dice stopped and shows $resultWord.")
}
