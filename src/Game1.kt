package engineering.nxt.rockpaperscisors.pt1

// Game.kt
fun main() {
  println("Hello.")
  print("Please choose between Rock [r], Paper [p] or Scissors [s]: ")
  val userChoice = readLine()
  println("You've chosen $userChoice.")
}
