// DoWhile.kt
fun main() {
    println("I will choose one of 'a', 'b' or 'c'.")
    val computerChoice = arrayOf("a", "b", "c").random()

    do {
        print("Can you guess what I've chosen? ")
        val playerChoice = readLine()
    } while (playerChoice != computerChoice)

    println("Yes, I've thought of '$computerChoice'.")
}
