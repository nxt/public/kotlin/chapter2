package engineering.nxt.rockpaperscisors.pt8

// Game.kt
class Scoreboard {
  var computerScore = 0
  var playerScore = 0

  fun printScore() {
    println("The computer won $computerScore times and the player $playerScore times.")
  }

  fun increaseComputerScore() {
    computerScore = computerScore + 1
  }

  fun increasePlayerScore() {
    playerScore = playerScore + 1
  }
}

fun playerDraw(): String {
  print("Hello. Please choose between Rock [r], Paper [p] or Scissors [s]: ")
  val userCommand = readLine()
  val userChoice =
    when (userCommand) {
      "r" -> "rock"
      "p" -> "paper"
      "s" -> "scissors"
      else -> "unknown"
    }
  println("You've chosen $userChoice.")

  return userChoice
}

fun computerDraw(): String {
  val choices = arrayOf("rock", "paper", "scissors")
  val computerChoice = choices.random()
  println("The computer has chosen $computerChoice.")

  return computerChoice
}

fun referee(userChoice: String, computerChoice: String, gameScore: Scoreboard) {
  val result = when(userChoice) {
    "rock" -> when(computerChoice) {
      "rock" -> "It's a tie"
      "paper" -> {
        gameScore.increaseComputerScore()
        "Paper beats rock, the computer wins."
      }
      "scissors" -> {
        gameScore.increasePlayerScore()
        "Rock beats scissors, the player wins."
      }
      else -> throw RuntimeException("Unknown choice.")
    }
    "paper" -> when(computerChoice) {
      "rock" -> {
        gameScore.increasePlayerScore()
        "Paper beats rock, the player wins."
      }
      "paper" -> "It's a tie"
      "scissors" -> {
        gameScore.increaseComputerScore()
        "Scissors beat paper, the computer wins."
      }
      else -> throw RuntimeException("Unknown choice.")
    }
    "scissors" -> when(computerChoice) {
      "rock" -> {
        gameScore.increaseComputerScore()
        "Rock beats scissors, the computer wins."
      }
      "paper" -> {
        gameScore.increasePlayerScore()
        "Scissors beat paper, the player wins."
      }
      "scissors" -> "It's a tie"
      else -> throw RuntimeException("Unknown choice.")
    }
    else -> throw RuntimeException("Unknown choice.")
  }

  println(result)
}

fun main() {
  val gameScore = Scoreboard()

  do {
    val userChoice = playerDraw()
    val computerChoice = computerDraw()

    referee(userChoice, computerChoice, gameScore)

    gameScore.printScore()

    // One more time?
    print("Would you like to play again? Type 'y' for yes and 'n' for no. ")
    val againResponse = readLine()
  } while (againResponse == "y")
}
