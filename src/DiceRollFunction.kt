package engineering.nxt.rockpaperscisors.dicerollfunction

// DiceRoll.kt
fun rollDice() { // new function
    val diceRoll = arrayOf(1,2,3,4,5,6).random()
    println("The dice shows $diceRoll.")
}

fun main() {
    do {
        rollDice() // call the existing code

        // One more time?
        print("Roll the dice again? Yes [y] or No [n]? ")
    } while (readLine() == "y")
}
